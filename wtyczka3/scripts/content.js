// content.js

// Function to read and send articles to the background script
function readAndSendArticles() {
  console.log('.');
  const articles = Array.from(document.querySelectorAll("h3")).map(article => article.innerText);
  console.log("Articles before sending:", articles);
  chrome.runtime.sendMessage({ type: 'sendArticles', articles });
}

// Function to replace headlines with modified ones
function replaceHeadlines(request) {
  console.log(request.headlines);
  const articles = document.querySelectorAll("h3");

  articles.forEach((article, index) => {
    article.innerText = request.headlines[index];
  });
}

// Check if the current URL is 'onet.pl'
if (window.location.href.includes('onet.pl')) {
  // Delay the execution by 5 seconds
  setTimeout(() => {
    // Call the function to read and send articles
    readAndSendArticles();
    console.log('..');

    // Listen for scrolling event to trigger the script again
    window.addEventListener('scroll', function() {
      // Check if user has scrolled to the bottom
      if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        // Call the function to read and send articles
        readAndSendArticles();
        console.log('..');
      }
    });
  }, 5000);
}


// Listen for messages from the background script
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.type === 'replaceHeadlines' && request.headlines) {
    replaceHeadlines(request);
  }
});

// Add interval to repeatedly execute readAndSendArticles every 3 seconds
//let x = setInterval(readAndSendArticles, 3000);
