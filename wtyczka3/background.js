// background.js

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.type === 'sendArticles' && message.articles) {
    console.log('Received articles:', message.articles);

    // Send the articles to the server for title generation
    fetch('http://localhost:5000/generateTitles', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ articles: message.articles }),
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);

        // Replace the original titles with the generated titles
        chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
          chrome.tabs.sendMessage(tabs[0].id, { type: 'replaceTitles', titles: res.generatedTitles });
        });
      })
      .catch(error => {
        console.error('Error communicating with server:', error.message);
      });
  }
  return true;
});
