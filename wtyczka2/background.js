// background.js

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.type === 'sendArticles' && message.articles) {

      console.log('Received articles:', message.articles);

    // Send the articles to the server
    fetch('http://localhost:5000/addTytul', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ articles: message.articles }),
    })

      .then(res => res.json())
      .then(res => {
      console.log(res)
        // Send the modified headlines back to the content script
        chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
          chrome.tabs.sendMessage(tabs[0].id, { type: 'replaceHeadlines', headlines: res.modifiedHeadlines });
        });
      })
      .catch(error => {
        console.error('Error communicating with server:', error.message);
      });
  }
  return true;
});
