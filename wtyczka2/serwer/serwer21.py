import logging
from flask import Flask, request, jsonify

app = Flask(__name__)

# Configure the logging module
logging.basicConfig(level=logging.DEBUG)

@app.route('/addTytul', methods=['POST'])
def add_tytul():
    data = request.get_json()
    articles = data.get('articles')

    # logging.info("UN-Modified Headlines: %s", articles)

    # Add 'tytul' in front of every headline
    modified_headlines = [article + article for article in articles]

    # Use the logging module to log the modified headlines
    # logging.info("Modified Headlines: %s", modified_headlines)

    # Send the modified headlines back to the background script
    return jsonify({'modifiedHeadlines': modified_headlines})

if __name__ == '__main__':
    app.run(debug=True)
